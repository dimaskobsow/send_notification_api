from django.urls import path, include
from django.shortcuts import redirect
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

router = DefaultRouter()
router.register(r'Clients', views.ClientViewSet)
router.register(r'Mailing', views.MailingSerializer)
router.register(r'Stats', views.StatsListSerializer)

urlpatterns = [
    path('', lambda request: redirect('/docs/', permanent=True)),
    path('api/', include(router.urls)),
    #path('api/stats/', views.StatsList.as_view()),
    #path('api/stats/<int:pk>/', views.StatsDetail.as_view()),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
