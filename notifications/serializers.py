from rest_framework import serializers

from .models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('id', 'phone_number', 'mobile_operator_code', 'tag', 'tz')

class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'start_datetime', 'end_datetime', 'message_text', 'filter_tag', 'filter_mobile_operator_code')

class StatisticMailingList(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'processed', 'all_messages', 'sent_messages')

class StatisticMailingDetailList(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'processed', 'all_messages', 'sent_messages', 'message_set')
        depth = 1

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'mailing', 'client', 'status', 'sent_time',)
        #depth = 1