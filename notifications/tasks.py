import logging

import requests
from django.utils import timezone
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Template, Context
from datetime import timedelta
from celery import shared_task
from .models import Client, Mailing, Message
import pytz
import environ

env = environ.Env()
environ.Env.read_env()

@shared_task()
def findMailingToSendNow():
    for m in Mailing.objects.filter(processed = False, start_datetime__lt = timezone.now(), end_datetime__gt = timezone.now()):
        create_messages_for_mailing.delay(mailingid=m.pk)


@shared_task(bind=True)
def create_messages_for_mailing(self, mailingid: int):
    """
    Создание сообщений для рассылки
    """
    # Получим рассылку, заодно проверим, что её ещё не удалили
    try:
        mailing = Mailing.objects.get(pk=mailingid)
    except:
        # тут должно быть логирование
        return
    # Не лишним будет ещё раз проверить, что рассылка акутальна. Мало ли когда Celery на самом деле будет выполнять задачу
    if not mailing.need_processed:
        return
    # Важно сразу это сделать, чтобы не вызвать второй раз эту процедуру
    mailing.processed = True
    mailing.save()
    clients = Client.objects.all()
    if mailing.filter_tag:
        clients = clients.filter(tag__iexact=mailing.filter_tag)
    if mailing.filter_mobile_operator_code:
        clients = clients.filter(mobile_operator_code__iexact=mailing.filter_mobile_operator_code)
    for client in clients:
        # Создание объектов сообщений можно внести в задачу.
        # Преимущества выбрнного метода - в процессе выполнения рассылки будет доступна статистика по прогрессу.
        # Будет информация об отменённых из-за временых рамок сообщениях.
        message= Message(mailing=mailing, client=client)
        message.save()
        #Добавим задачу на выполнение рассылки.
        send_message.delay(msg_id=message.pk)


@shared_task(bind=True)
def send_message(self, msg_id) :
    """
    Отправка сообщения
    """
    try :
        msg: Message = Message.objects.get(pk=msg_id)
    except Exception as e :
        logging.warning("Can't get message {}", format(msg_id))
        return

    # Проверка на время.
    if msg.mailing.start_datetime > timezone.now() :
        # Ещё не наступило время начала рассылки
        # технически мы не должны тут оказаться кроме одного случая
        # наступило время рассылки, рассылка началась, сообщение ещё не отправлено
        # в это время рассылка изменена.
        # Считаю верным запретить редактировать уже начавшиеся рассылки, максимум отменять/дублировать
        # Но это не соответсвует ТЗ : )
        return self.retry(eta=msg.mailing.start_datetime)
    if msg.mailing.end_datetime < timezone.now() :
        # Закончиловсь время расскли, сообщение не актуально. Отменяем.
        msg.status = Message.STATUS_CANCELED
        msg.save()
        logging.info("Message {} canceled. Expired time".format(msg_id))
        return
    # Проверка на локальное время. Вдруг сейчас у пользователя ночь, а данная рассылка должна приходить только днём
    if msg.mailing.start_time or msg.mailing.end_time:
        # Алтерантивой можно ставить принудильно во время сохранения ставить время от начала до конца дня, вместо того чтобы проверять, что они заданы
        timezone.activate(pytz.timezone(msg.client.tz))
        local_time = timezone.localtime(timezone.now()).time()
        can_sand = True
        if not msg.mailing.end_time and msg.mailing.start_time:
            if local_time < msg.mailing.start_time:
                can_sand = False
        elif msg.mailing.end_time and not msg.mailing.start_time:
            if local_time > msg.mailing.end_time:
                can_sand = False
        elif (msg.mailing.start_time < msg.mailing.end_time and (
                local_time < msg.mailing.start_time or local_time > msg.mailing.end_time)) \
                or (msg.mailing.start_time > msg.mailing.end_time and (
                local_time < msg.mailing.start_time and local_time > msg.mailing.end_time)) :
            # Если захочешь поменять на изящнее, то помни что может быть такой случай:
            # начало в 22, конец в 6 утра. Ночная рассылка.
            can_sand = False
        if not can_sand:
            msg.status = Message.STATUS_CANCELED
            # ПО ТЗ рассылка отменяется в таком случае.
            # Я бы предложил проверить нельзя ли всё же выслать позже. Есть ли интервал когда и клиент доступн и срок рассылки не истёк
            # Или втупую кинуть на первое дуоступное время у клиента таску,а если к тому времени рассылка истечёт, то она и отменится.
            # Но лучше всё же проверить - будет акутальнее статистика и меньше мусора
            msg.save()
            logging.info("Message {} canceled. Not good client time".format(msg_id))
            return

    # Собственно отправка
    endpoint = env('SEND_MESSAGE_ENDPOINT') + str(msg_id)
    headers = {"Authorization" : "Bearer {}".format(env('SEND_MESSAGE_KEY')),
               'Content-Type' : 'application/json',
               'accept' : 'application/json',
               }
    data = {
        'id' : msg.pk,
        'phone' : msg.client.phone_number,
        'text' : msg.mailing.message_text,
    }
    try:
        r = requests.post(endpoint, json=data, headers=headers)
    except Exception as e :
        logging.error("Message {} error. Exception {}".format(msg_id, e))
        msg.status = Message.STATUS_RETRY
        msg.save()
        raise self.retry(exc=e, retry_backoff_max=3600, retry_backoff=True, max_retries=None)
    if r.status_code != 200 :
        logging.error("Message {} error. Code {}. r.".format(msg_id, r.status_code))
        msg.status = Message.STATUS_RETRY
        msg.save()
        raise self.retry(retry_backoff=60, retry_backoff_max=3600, max_retries=None)
    else:
        msg.status = Message.STATUS_SENT
        msg.sent_time = timezone.now()
        msg.save()
        logging.info("Message {} sent".format(msg_id))

@shared_task(bind=True)
def send_stats_email(self):
    subject = 'Stats notification Sender ' +str(timezone.now().date())
    template_str = '''Today and yestrday mailings
    <table border="1">
    <tr>
        <th>id</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Processed</th>
        <th>All messages</th>
        <th>Sent messages</th>
    </tr>
    {% for mailing in mailings%}
        <tr>
            <td>{{mailing.id}}</td>
            <td>{{mailing.start_datetime}}</td>
            <td>{{mailing.end_datetime}}</td>
            <td align="center">{{mailing.processed}}</td>
            <td align="center">{{mailing.all_messages}}</td>
            <td align="center">{{mailing.sent_messages}}</td>
        </tr>
    {% endfor%}
    </table>
    '''
    template = Template(template_str)
    mailings = Mailing.objects.filter(start_datetime__gte = timezone.now().date()-timedelta(days=1), start_datetime__lt = timezone.now().date()+timedelta(days=1)).order_by('start_datetime')
    text_content = 'Stats'
    html_content = template.render(context=Context({'mailings' : mailings}))
    from_email = env('FROM_EMAIL')
    to = env('EMAIL_TO_STATS')
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    # send_mail(
    #         theme,
    #         body,
    #         'from@apisender.com',
    #         ['vel.pavel@gmail.com'],
    #         fail_silently=False,
    #     )