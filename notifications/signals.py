from django.db.models import signals
from .models import Mailing

from .tasks import create_messages_for_mailing

def mailing_post_save(sender, instance:Mailing, signal, *args, **kwargs):
    if instance.need_processed:
        create_messages_for_mailing.delay(mailingid=instance.pk)

signals.post_save.connect(mailing_post_save, sender=Mailing)