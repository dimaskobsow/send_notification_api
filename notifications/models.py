from django.db import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.conf import settings
from django.utils import timezone

import pytz

phone_code_validator = RegexValidator(regex=r'^\d{3}$', message="The mobile code operator must be 3 digits")
phone_number_validator = RegexValidator(regex=r'^7\d{10}$',
                                        message="The phone must be in format 7XXXXXXXXXX (X - digit 1-9)")
def validate_timezone(value):
    if value not in pytz.all_timezones:
        raise ValidationError('Incorrect timezone format')


class Mailing(models.Model):
    start_datetime = models.DateTimeField(verbose_name='Start date', help_text='Datetime when mailing will be start.')
    end_datetime = models.DateTimeField(verbose_name='End date', help_text='Datetime when mailing will be force stopped.')
    message_text = models.TextField(verbose_name='Message')
    filter_tag = models.CharField(verbose_name='Tag filter', max_length=80, blank=True)
    filter_mobile_operator_code = models.CharField(verbose_name='Mobile operator filter', max_length=3,
                                         validators=[phone_code_validator], blank=True)
    start_time = models.TimeField(verbose_name='Earliest time to send', null=True, blank=True)
    end_time = models.TimeField(verbose_name='Latest time to send', null=True, blank=True)
    processed = models.BooleanField(editable=False, verbose_name='Mailing started', default=False) #Служебный флаг запущена ли рассылка.

    @property
    def need_processed(self):
        return not self.processed and self.start_datetime < timezone.now() and self.end_datetime > timezone.now()

    def all_messages(self):
        return self.message_set.count()

    def sent_messages(self):
        return self.message_set.filter(status=Message.STATUS_SENT).count()

    def __str__(self) :
        return 'Mailing {0.id} at {0.start_datetime}'.format(self)

    def save(self, *args, **kwargs):
        # Если отредактирована уже своершённая рассылка так, что дата в будуем, то её надо повторить.
        # Хотя это плохой кейс, который делает статистику менее понятной - будут учтены сообщения всех итераций рассылки.
        # Плюс потенциально неявное поведение если рассылка уже началась, но ещё не завершилась
        if self.processed and self.start_datetime > timezone.now():
            self.processed = False
        return super(Mailing, self).save(*args, **kwargs)


class Client(models.Model):
    phone_number = models.CharField(verbose_name='Phone number', max_length=11, unique=True,
                                    validators=[phone_number_validator])
    mobile_operator_code = models.CharField(verbose_name='Mobile operator', max_length=3,
                                         validators=[phone_code_validator], blank=True)
    tag = models.CharField(verbose_name='Tag', max_length=80, blank=True)
    # На текущий момент самая "длинная" tz = 32 символам. Заложим небольшой запас
    tz = models.CharField(verbose_name='Timezone', max_length=40, validators=[validate_timezone],
                          default=settings.TIME_ZONE)

    def save(self, *args, **kwargs) :
        if not self.mobile_operator_code:
            self.mobile_operator_code = str(self.phone_number)[1 :4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self) :
        return 'Client  {0.phone_number}'.format(self)

class Message(models.Model):
    STATUS_WAIT = 'wait'
    STATUS_SENT = 'sent'
    STATUS_CANCELED = 'canceled'
    STATUS_RETRY = 'retry' # была ошибка пробуем повторить

    STATUSES = [
        (STATUS_WAIT, 'wait'),
        (STATUS_SENT, 'sent'),
        (STATUS_CANCELED, 'canceled'),

    ]

    sent_time = models.DateTimeField(verbose_name='Sent time', null=True)
    status = models.CharField(max_length=10,
                              choices=STATUSES,
                              default=STATUS_WAIT,
                              verbose_name='Current status')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='Mailing')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Client')

    def sent_time_options(self):
        pass