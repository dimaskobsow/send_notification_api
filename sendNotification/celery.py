import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sendNotification.settings')

app = Celery('sendNotification')
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

# @app.on_after_configure.connect
# def setup_periodic_tasks(sender, **kwargs):
#     sender.add_periodic_task(60.0, 'notifications.tasks.findMailingToSendNow')

app.conf.beat_schedule = {
    'every-minute': {
        'task': 'notifications.tasks.findMailingToSendNow',
        'schedule': crontab(),
    },
    'every-day-statsexit()-email': {
        'task': 'notifications.tasks.send_stats_email',
        'schedule': crontab(minute=0, hour=11),
    },
}
